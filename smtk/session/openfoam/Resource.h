//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_openfoam_Resource_h
#define __smtk_session_openfoam_Resource_h

#include "smtk/session/openfoam/Exports.h"
#include "smtk/session/openfoam/Session.h"

#include "smtk/model/Resource.h"

#include "smtk/resource/DerivedFrom.h"
#include "smtk/resource/Manager.h"
#include "smtk/resource/Resource.h"

namespace smtk
{
namespace session
{
namespace openfoam
{

class SMTKOPENFOAMSESSION_EXPORT Resource
  : public smtk::resource::DerivedFrom<Resource, smtk::model::Resource>
{
public:
  smtkTypeMacro(smtk::session::openfoam::Resource);
  smtkSharedPtrCreateMacro(smtk::resource::Resource);

  virtual ~Resource() override;

  Session::Ptr session() const { return m_session; }

  void setSession(const Session::Ptr& session);

protected:
  Resource(const smtk::common::UUID&, smtk::resource::Manager::Ptr manager = nullptr);
  Resource(smtk::resource::Manager::Ptr manager = nullptr);

  Session::Ptr m_session;
};
}
}
}

#endif
