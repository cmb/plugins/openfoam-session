//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_openfoam_Session_h
#define __smtk_session_openfoam_Session_h

#include "smtk/session/openfoam/Exports.h"

#include "smtk/model/EntityRef.h"
#include "smtk/model/Session.h"

namespace smtk
{
namespace model
{

class ArrangementHelper;
}
}

namespace smtk
{
namespace session
{
namespace openfoam
{

class SMTKOPENFOAMSESSION_EXPORT Session : public smtk::model::Session
{
public:
  smtkTypeMacro(Session);
  smtkSuperclassMacro(smtk::model::Session);
  smtkSharedFromThisMacro(smtk::model::Session);
  smtkCreateMacro(smtk::model::Session);
  typedef smtk::model::SessionInfoBits SessionInfoBits;

  virtual ~Session() override;

  const std::string& workingDirectory() const { return m_workingDirectory; }
  void setWorkingDirectory(const std::string wd) { m_workingDirectory = wd; }

  void createWorkingDirectory() const;
  void removeWorkingDirectory() const;
  bool workingDirectoryExists() const;

protected:
  Session();

private:
  std::string m_workingDirectory;

  Session(const Session&);        // Not implemented.
  void operator=(const Session&); // Not implemented.
};

} // namespace openfoam
} // namespace session
} // namespace smtk

#endif // __smtk_session_openfoam_Session_h
