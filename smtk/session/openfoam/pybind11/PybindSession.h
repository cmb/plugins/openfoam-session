//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_openfoam_Session_h
#define pybind_smtk_session_openfoam_Session_h

#include <pybind11/pybind11.h>

#include "smtk/session/openfoam/Session.h"

#include "smtk/model/Session.h"

namespace py = pybind11;

PySharedPtrClass< smtk::session::openfoam::Session, smtk::model::Session > pybind11_init_smtk_session_openfoam_Session(py::module &m)
{
  PySharedPtrClass< smtk::session::openfoam::Session, smtk::model::Session > instance(m, "Session");
  instance
    .def("shared_from_this", (std::shared_ptr<smtk::session::openfoam::Session> (smtk::session::openfoam::Session::*)()) &smtk::session::openfoam::Session::shared_from_this)
    .def("shared_from_this", (std::shared_ptr<const smtk::session::openfoam::Session> (smtk::session::openfoam::Session::*)() const) &smtk::session::openfoam::Session::shared_from_this)
    .def_static("create", (std::shared_ptr<smtk::session::openfoam::Session> (*)()) &smtk::session::openfoam::Session::create)
    .def_static("create", (std::shared_ptr<smtk::session::openfoam::Session> (*)(::std::shared_ptr<smtk::session::openfoam::Session> &)) &smtk::session::openfoam::Session::create, py::arg("ref"))
    .def_static("staticClassName", &smtk::session::openfoam::Session::staticClassName)
    .def("name", &smtk::session::openfoam::Session::name)
    .def("className", &smtk::session::openfoam::Session::className)
    .def("workingDirectory", &smtk::session::openfoam::Session::workingDirectory)
    .def("setWorkingDirectory", &smtk::session::openfoam::Session::setWorkingDirectory)
    .def("createWorkingDirectory", &smtk::session::openfoam::Session::createWorkingDirectory)
    .def("removeWorkingDirectory", &smtk::session::openfoam::Session::removeWorkingDirectory)
    .def("workingDirectoryExists", &smtk::session::openfoam::Session::workingDirectoryExists)
    .def_static("CastTo", [](const std::shared_ptr<smtk::model::Session> i) {
        return std::dynamic_pointer_cast<smtk::session::openfoam::Session>(i);
      })
    ;
  return instance;
}

#endif
