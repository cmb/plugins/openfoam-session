//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#ifndef __smtk_smtk_session_openfoam_ImportSTLFile_h
#define __smtk_smtk_session_openfoam_ImportSTLFile_h

#include "smtk/session/openfoam/Exports.h"
#include "smtk/PublicPointerDefs.h"

#include <string>

class vtkDataSet;

namespace smtk
{
namespace mesh
{
class MeshSet;
}
}

namespace smtk
{
namespace session
{
namespace openfoam
{

//Import an STL file to a smtk::mesh::resource.
class SMTKOPENFOAMSESSION_EXPORT ImportSTLFile
{
public:
  explicit ImportSTLFile();

  //Import an STL file as a collection.
  smtk::mesh::ResourcePtr operator()(
    const std::string& filename, const smtk::mesh::InterfacePtr& interface) const;

  bool operator()(const std::string& filename, smtk::mesh::ResourcePtr resource) const;
};
}
}
}

#endif //__smtk_session_openfoam_ImportSTLFile_h
